variable "prefix" {
  default = "diaa"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "diaa.ramahi@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
  default     = "terraform"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
  default     = "Di115ramahi"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "363425050455.dkr.ecr.us-east-1.amazonaws.com/django-service:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "363425050455.dkr.ecr.us-east-1.amazonaws.com/proxy-gateway:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
  default     = "4^q$4#vukj@jem!u35u+!mpf5@-)9ij#4ppnf@-7h7q11_n!!c"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "ammanmart.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
